from collections import defaultdict

import pytest
import pytest_mock

from assertion_helper import *

class test_helper:
    def it_handles_dictionaries(self):
        assert isinstance(helper(dict()), MappingAssertion)
        assert isinstance(helper(defaultdict()), MappingAssertion)

    def it_handles_lists(self):
        assert isinstance(helper(list()), SequenceAssertion)
        assert not isinstance(helper(set()), SequenceAssertion)

    def it_handles_iterables(self):
        value = {1, 2, 3}

        assert isinstance(helper(value), IterableAssertion)
        assert isinstance(helper(x for x in value), IterableAssertion)
        assert isinstance(helper(range(10)), IterableAssertion)
        assert isinstance(helper(map(id, value)), IterableAssertion)
        assert isinstance(helper(filter(id, value)), IterableAssertion)
        assert isinstance(helper(iter(id, value)), IterableAssertion)

    def it_handles_numbers(self):
        assert isinstance(helper(1), NumericAssertion)
        assert isinstance(helper(1.0), NumericAssertion)
        assert isinstance(helper(1+2j), NumericAssertion)

    def it_handles_mocks(self, mocker: pytest_mock.MockFixture):
        mocker.patch.object(Assertion, "__repr__")

        assert isinstance(helper(mocker.Mock()), MockAssertion)
        assert isinstance(helper(Assertion.__repr__), MockAssertion)

