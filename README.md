# Assertion Helper [![build][]][status] [![coverage][]][report]
To make your assertions easier to read. It allows for structual equality
testing between objects, ditionary subset testing and a bunch more.

```python
import mock

# Using `_` á la Djangos translation helper
from ah import helper as _

assert { "key": "spam" } in _({ "abc": 123, "key": "spam" })

fn = mock.Mock()
fn(1, 2, key="word")
assert _(fn) == mock.call(1, 2, key="word")

class Spam:
  def __init__(self, bar):
    self.bar = bar

ham = Spam("ham")
egg = Spam("egg")
assert ham != egg
assert _(ham) == egg

assert (0.1 + 0.2) != 0.3
assert (0.1 + 0.2) == _(0.3)
```

One caveat is that due to the operator precedence/evaluation rules, it's
usually required for the assertion helper to be on the left side to take
precedence.

[build]: https://gitlab.com/maxnordlund/ah/badges/master/build.svg
[status]: https://gitlab.com/maxnordlund/ah/commits/master
[coverage]: https://gitlab.com/maxnordlund/ah/badges/master/coverage.svg
[report]: https://gitlab.com/maxnordlund/ah/commits/master
