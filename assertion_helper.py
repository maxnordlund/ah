from functools import wraps, reduce
from itertools import repeat
from typing import (
    Callable,
    Iterable,
    Mapping,
    Sequence,
    Optional,
    Union,
    Text,
    TypeVar
)

import mock
import pytest

T = TypeVar("T")
K = TypeVar("T")
V = TypeVar("T")
Numeric = Union[int, float, complex]

_builtin_types = tuple(
    typ
    for typ in __builtins__.values()
    if type(type) is type and not issubclass(type, BaseException)
)

def helper(expected):
    if isinstance(expected, Assertion):
        return expected
    elif isinstance(expected, mock.Mock):
        return MockAssertion(expected)
    elif isinstance(expected, Mapping):
        return MappingAssertion(expected)
    elif isinstance(expected, Sequence):
        return SequenceAssertion(expected)
    elif isinstance(expected, Iterable):
        return IterableAssertion(expected)
    elif Assertion.is_numeric(expected):
        return NumericAssertion(expected)
    else:
        return Assertion(expected)

def dualmethod(method):
    """
    Decorator for a method being called both as a static and instance method.
    """
    @wraps(method)
    def wrapper(maybe_self: Optional["Assertion"], value=None):
        if value is None:
            value = getattr(maybe_self, "expected", maybe_self)
        return method(value)

    return wrapper

def proxymethod(method):
    """
    Decorator for a method that prefers proxying the call to the ``expected``
    value, otherwise falls back to the given default implementetion.
    """
    name = method.__name__

    @wraps(method)
    def wrapper(self: "Assertion", *args, **kwargs):
        target_method = getattr(self.expected, name, None)

        if isinstance(target_method, Callable):
            return target_method(*args, **kwargs)
        else:
            return method(*args, **kwargs)

    return wrapper


class Assertion:
    def __init__(self, expected):
        self.expected = expected

    def __eq__(self, actual):
        if self.expected == actual:
            return True
        elif type(actual) is Assertion: # Ensure we don't recurse infinitely
            return self.expected == actual.expected
        elif self.is_mapping(actual):
            return MappingAssertion(actual) == self.expected.__dict__
        elif self.is_builtin(actual):
            return helper(actual) == self.expected
        else:
            return MappingAssertion(actual.__dict__) == self.expected.__dict__

    def __repr__(self):
        return f"_({self.expected!r})"

    @dualmethod
    def is_builtin(value):
        return isinstance(value, _builtin_types)

    @dualmethod
    def is_call(value):
        return isinstance(value, type(mock.call))

    @dualmethod
    def is_iterable(value):
        return isinstance(value, Iterable)

    @dualmethod
    def is_mapping(value):
        return isinstance(value, Mapping)

    @dualmethod
    def is_mock(value):
        return isinstance(value, mock.Mock)

    @dualmethod
    def is_numeric(value):
        return isinstance(value, Numeric.__args__) \
               and not isinstance(value, bool)

    @dualmethod
    def is_sequence(value):
        return isinstance(value, Sequence)


class _CallCount:
    def __init__(self, mock: mock.Mock):
        self.mock = mock

    def __eq__(self, expected):
        __tracebackhide__ = True
        if expected == 1:
            self.mock.assert_called_once()
            return True
        else:
            return self.mock.call_count == expected

    def __int__(self):
        return self.mock.call_count


class MockAssertion(Assertion):
    def __init__(self, expected: mock.Mock):
        self.expected = expected

    @property
    def count(self):
        return _CallCount(self.expected)

    @property
    def called(self):
        __tracebackhide__ = True
        self.expected.assert_called()
        return True

    @property
    def not_called(self):
        __tracebackhide__ = True
        self.expected.assert_not_called()
        return True

    def __bool__(self):
        return self.expected.called

    def __eq__(self, actual):
        __tracebackhide__ = True
        if self.is_call(actual):
            args, kwargs = self._extract_call_args(actual)
            self.expected.assert_called_once_with(*args, **kwargs)
            return True
        else:
            return False

    def __contains__(self, actual):
        __tracebackhide__ = True
        if self.is_call(actual):
            args, kwargs = self._extract_call_args(actual)
            self.expected.assert_any_call(*args, **kwargs)
            return True
        elif self.is_iterable(actual):
            self.expected.assert_has_calls(
                map(self._extract_call_args, actual),
                any_order=(not self.is_sequence(actual))
            )
            return True
        else:
            return False

    def __getitem__(self, key):
        if isinstance(key, tuple):
            return map(self.__getitem__, key)
        elif isinstance(key, slice):
            args, kwargs = self._extract_call_args(
                self.expected.call_args_list[key.start]
            )
            key = key.stop
        else:
            args, kwargs = self._extract_call_args(self.expected.call_args)

        if isinstance(key, int):
            return args[key]
        elif isinstance(key, Text):
            return kwargs[key]
        else:
            raise TypeError(
                f"indices must be integers, strings, slices or tuples, not {type(key)}"
            )

    def __len__(self):
        return len(self.expected.call_args_list)

    def _extract_call_args(self, call):
        if len(call) == 3:
            _name, args, kwargs = call
        else:
            args, kwargs = call

        return args, kwargs


class MappingAssertion(Assertion):
    def __init__(self, expected: Mapping[K, V]):
        self.expected = expected

    def __eq__(self, actual):
        if actual == self.expected:
            return True
        elif self.is_mapping(actual):
            return len(actual) == len(self.expected) and actual in self
        else:
            return False

    def __contains__(self, actual):
        if actual in self.expected or actual in self.values():
            return True
        elif self.is_mapping(actual):
            for key in actual:
                if key in self.expected and actual[key] == self.expected[key]:
                    continue
                else:
                    return False
            return True
        elif self.is_iterable(actual):
            for value in actual:
                if value in self: # Recurse
                    continue
                else:
                    return False
            return True
        else:
            return False

    def __getitem__(self, key):
        if isinstance(key, tuple):
            return map(self.__getitem__, key)
        else:
            return super().__getitem__(key)

    @proxymethod
    def __len__(self):
        length = 0
        for _ in self.expected:
            length += 1

        return length

    @proxymethod
    def values(self) -> Iterable[V]:
        for key in self.expected:
            yield self.expected[key]

    @proxymethod
    def keys(self) -> Iterable[K]:
        for key in self.expected:
            yield key


class IterableAssertion(Assertion):
    MAX_COMPARISONS = 1000

    def __init__(self, expected: Iterable[T]):
        self.expected = expected

    def __eq__(self, actual):
        return actual == self.expected

    def __contains__(self, actual):
        if actual == self.expected:
            return True

        if self.is_iterable(actual):
            stream = zip(self.expected, actual)
        else:
            stream = zip(self.expected, repeat(actual))

        comparisons = 0
        for left, right in stream:
            comparisons += 1

            if left == right:
                continue
            elif comparisons > IterableAssertion.MAX_COMPARISONS:
                raise TypeError("Too many values to compare")
            else:
                return False
        return False

    def any(self, fn):
        for value in self.expected:
            if fn(value):
                return True
        assert False, "None of the expected values satisfies fn"

    def all(self, fn):
        for value in self.expected:
            assert fn(value) # Fails with the value that doesn't satisfy fn
        return True

    def filter(self, fn):
        return self.__class__(filter(fn, self.expected))

    def map(self, fn):
        return self.__class__(map(fn, self.expected))

    def reduce(self, fn, initializer=None):
        return helper(reduce(fn, self.expected, initializer))


class SequenceAssertion(IterableAssertion):
    def __init__(self, expected: Sequence[T]):
        self.expected = expected

    def __eq__(self, actual):
        if self.is_sequence(actual) and len(self) != len(actual):
            return False
        else:
            return actual in self

    @proxymethod
    def __len__(self):
        return NotImplemented


class NumericAssertion(Assertion):
    def __init__(self, expected: Numeric):
        self.expected = expected

    def __eq__(self, actual: Numeric):
        return actual == pytest.approx(self.expected)

    def __contains__(self, actual: Numeric):
        return abs(actual) < self.expected
